<?php

declare(strict_types=1);

namespace ZdenekGebauer\Pagination;

class Renderer
{

    /**
     * @var Calculator
     */
    private $calculator;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $cssLinkSelected = 'selected';

    /**
     * @var string
     */
    private $linksDelimiter = ' ';

    /**
     * @var string
     */
    private $pageDescription = '%d';

    /**
     * @var array<string, int|string>
     */
    private $queryParams = [];

    /**
     * @var string
     */
    private $positionVariable;

    /**
     * @var int
     */
    private $posType;

    public function __construct(
        Calculator $calculator,
        string $baseUrl,
        string $positionVariable = Constants::POSITION_VARIABLE,
        int $posType = Constants::POSITION_AS_PARAMETER
    ) {
        $this->calculator = $calculator;
        $this->baseUrl = $baseUrl;
        $this->positionVariable = $positionVariable;
        $this->posType = $posType;
    }

    public function setCssLinkSelected(string $cssLinkSelected): void
    {
        $this->cssLinkSelected = $cssLinkSelected;
    }

    public function setLinksDelimiter(string $linksDelimiter): void
    {
        $this->linksDelimiter = $linksDelimiter;
    }

    /**
     * Parameter should contains placeholder "%d". Placeholder will e replace by page number
     *
     * @param string $pageDescription
     */
    public function setPageDescription(string $pageDescription): void
    {
        $this->pageDescription = $pageDescription;
    }

    /**
     * sets additional parameters for including in url
     *
     * @param array<string, int|string> $params
     */
    public function setQueryParams(array $params): void
    {
        $this->queryParams = [];
        foreach ($params as $key => $value) {
            $this->queryParams[trim($key)] = trim((string)$value);
        }
    }

    /**
     * @param int $range max number of visible links before/after current position
     * @return string links to pages
     */
    public function getLinks(int $range = 10): string
    {
        return $this->getLinksByTemplate('<a href="URL" class="CSS" LINK_ATTRIBUTES>PAGE</a>', $range);
    }

    /**
     * example of template link with uppercase placeholders:
     * <li class="pagination__page CSS"><a href="URL" aria-label="Go to page PAGE" LINK_ATTRIBUTES>PAGE</a></li>
     *
     * @param string $templateLink
     * @param int $range max number of visible links before/after current position
     * @return string links to pages
     */
    public function getLinksByTemplate(string $templateLink, int $range = 10): string
    {
        [$start, $end] = $this->calculatePagesRange($range);
        $links = [];
        $cssCurrent = empty($this->cssLinkSelected) ? 'selected' : $this->cssLinkSelected;
        for ($counter = $start; $counter < $end; $counter++) {
            $isCurrentPage = ($this->calculator->getPages()[$counter] === ($this->calculator->getCurrentPosition() + 1)
                || $this->calculator->getPages()[$counter] === $this->calculator->getCurrentPosition());
            $link = str_replace('URL', $this->createUrl($this->calculator->getPages()[$counter]), $templateLink);
            $link = str_replace('PAGE', (string)($counter + 1), $link);
            $link = str_replace('CSS', ($isCurrentPage ? $cssCurrent : ''), $link);
            $link = str_replace('LINK_ATTRIBUTES', ($isCurrentPage ? 'aria-current="page"' : ''), $link);
            $links[] = $link;
        }
        return implode($this->linksDelimiter, $links);
    }

    private function createUrl(int $pos): string
    {
        $url = $this->baseUrl;
        if ($pos > 1) {
            switch ($this->posType) {
                case Constants::POSITION_AS_STRING:
                    $url .= '/' . $this->positionVariable . '-' . $pos;
                    break;
                case Constants::POSITION_AS_DIRECTORY:
                    $url .= '/' . $this->positionVariable . '/' . $pos;
                    break;
                default:
                    $url .= (strpos($url, '?') === false ? '?' : '&')
                        . http_build_query([$this->positionVariable => $pos]);
                    break;
            }
        }
        $queryString = http_build_query($this->queryParams);
        if (!empty($queryString)) {
            $url .= (strpos($url, '?') === false ? '?' : '&');
        }

        return $url . $queryString;
    }

    public function getOptions(int $range = 10, bool $onlyPosition = false): string
    {
        $ret = '';
        [$start, $end] = $this->calculatePagesRange($range);

        for ($counter = $start; $counter < $end; $counter++) {
            $selected = $this->calculator->getCurrentPageNo() === $counter ? ' selected="selected"' : '';
            $position = $this->calculator->getPages()[$counter];
            if ($onlyPosition) {
                $value = ($position > 1 ? $position : '');
            } else {
                $value = $this->createUrl($position);
            }
            $linkDescription = sprintf($this->pageDescription, ($counter + 1));
            $ret .= '<option value="' . $value . '"' . $selected . '>' . $linkDescription . '</option>';
        }
        return $ret;
    }

    /**
     * @param int|null $range
     * @return array<int, int>
     */
    private function calculatePagesRange(?int $range = null): array
    {
        $start = 0;
        $countPages = count($this->calculator->getPages());
        $end = count($this->calculator->getPages());
        if ($range > 0 && $end > (2 * $range)) {
            $start = $this->calculator->getCurrentPageNo() - $range;
            $start = ($start < 0 ? 0 : $start);
            $end = $this->calculator->getCurrentPageNo() + $range + 1;
            $end = min([$end, $countPages]);
        }
        return [$start, $end];
    }

    /**
     * returns url to previous page of list or empty string
     *
     * @return string
     */
    public function getUrlPrev(): string
    {
        return $this->calculator->getPrevPosition() <= 0 && $this->calculator->getCurrentPosition() <= 1
            ? '' : $this->createUrl($this->calculator->getPrevPosition());
    }

    /**
     * returns url to next page of list or empty string
     *
     * @return string
     */
    public function getUrlNext(): string
    {
        return $this->calculator->getNextPosition() <= 0 ? '' : $this->createUrl($this->calculator->getNextPosition());
    }

    /**
     * returns string url to first page of list
     *
     * @return string
     */
    public function getUrlFirst(): string
    {
        return $this->calculator->getCurrentPosition() <= 1 || empty($this->calculator->getPages())
            ? '' : $this->createUrl(1);
    }

    /**
     * returns url to last page of list or empty string
     *
     * @return string
     */
    public function getUrlLast(): string
    {
        return $this->calculator->getLastPosition() <= 0 ? '' : $this->createUrl($this->calculator->getLastPosition());
    }

    /**
     * returns url to current page
     *
     * @return string
     */
    public function getUrlCurrent(): string
    {
        return $this->createUrl($this->calculator->getCurrentPosition() + 1);
    }
}
