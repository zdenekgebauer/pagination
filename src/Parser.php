<?php

declare(strict_types=1);

namespace ZdenekGebauer\Pagination;

class Parser
{

    /**
     * check if position is out of list (next page not exists)
     *
     * @param int $position
     * @param int $rowsPerPage
     * @param int $total
     * @return bool
     */
    public static function isPositionOutOfRange(int $position, int $rowsPerPage, int $total): bool
    {
        return ($position <= 1 || ($position + $rowsPerPage - 1) > (ceil($total / $rowsPerPage) * $rowsPerPage));
    }

    /**
     * parse value of position from url
     *
     * @param string $url url with position, if not set use current url
     * @param int $posType type of pagination in url
     * @param string $posVar name of variable holding position
     * @return int position, 1 if position is not recognized
     */
    public static function parsePosition(
        string $url,
        int $posType = Constants::POSITION_AS_PARAMETER,
        string $posVar = Constants::POSITION_VARIABLE
    ): int {
        $ret = 1;
        /** @var array<string, string> $parts */
        $parts = parse_url($url);
        $path = $parts['path'] ?? '';
        $queryString = $parts['query'] ?? '';
        switch ($posType) {
            case Constants::POSITION_AS_STRING:
                if (preg_match('/' . $posVar . '-([0-9]+)/s', $path, $matches)) {
                    $ret = (int)$matches[1];
                }
                break;
            case Constants::POSITION_AS_DIRECTORY:
                if (preg_match('/' . $posVar . '\/([0-9]+)/s', $path, $matches)) {
                    $ret = (int)$matches[1];
                }
                break;
            case Constants::POSITION_AS_PARAMETER:
            default:
                $tmp = [];
                parse_str($queryString, $tmp);
                $ret = (int)($tmp[$posVar] ?? 1);
        }
        return max([1, $ret]);
    }
}
