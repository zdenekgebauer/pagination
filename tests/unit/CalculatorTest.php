<?php

declare(strict_types=1);

namespace ZdenekGebauer\Pagination;

class CalculatorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConstructor(): void
    {
        $pager = new Calculator(100);
        $this->tester->assertEquals(0, $pager->getCurrentPosition());
        $this->tester->assertEquals(10, $pager->getEndPosition());
        $this->tester->assertEquals(90, $pager->getLastPosition());
        $this->tester->assertEquals(0, $pager->getPrevPosition());
        $this->tester->assertEquals(10, $pager->getNextPosition());

        $pager = new Calculator(100, 50, 20);
        $this->tester->assertEquals(50, $pager->getCurrentPosition());
    }

    public function testRecalculate(): void
    {
        $pager = new Calculator(100, 50, 10);
        $this->tester->assertEquals(60, $pager->getEndPosition());
        $this->tester->assertEquals(90, $pager->getLastPosition());
        $this->tester->assertEquals(41, $pager->getPrevPosition());
        $this->tester->assertEquals(60, $pager->getNextPosition());

        $pager = new Calculator(9, 0, 10);
        $this->tester->assertEquals(0, $pager->getLastPosition());
    }

    public function testGetPages(): void
    {
        $pager = new Calculator(50, 1, 10);
        $expect = [0, 10, 20, 30, 40];
        $this->tester->assertEquals($expect, $pager->getPages());
        $pager = new Calculator(0, 0, 10);
        $this->tester->assertEquals([], $pager->getPages());
    }

    public function testGetRecords(): void
    {
        $pager = new Calculator(20, 10, 3);
        $expect = [9, 11];
        $this->tester->assertEquals($expect, $pager->getRecords());
        $pager = new Calculator(20, 15, 10);
        $expect = [10, 19];
        $this->tester->assertEquals($expect, $pager->getRecords());
    }
}
