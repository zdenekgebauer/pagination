<?php

declare(strict_types=1);

namespace ZdenekGebauer\Pagination;

class RendererTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testUrls(): void
    {
        $pager = new Calculator(1, 1, 10);
        $renderer = new Renderer($pager, 'http://example.org');
        $this->tester->assertEquals('', $renderer->getUrlPrev());
        $this->tester->assertEquals('', $renderer->getUrlNext());
        $this->tester->assertEquals('', $renderer->getUrlFirst());
        $this->tester->assertEquals('', $renderer->getUrlLast());

        $pager = new Calculator(100, 50, 10);
        $renderer = new Renderer($pager, 'http://example.org');
        $renderer->setQueryParams(['par' => 'value']);
        $this->tester->assertEquals('http://example.org?p=41&par=value', $renderer->getUrlPrev());
        $this->tester->assertEquals('http://example.org?p=60&par=value', $renderer->getUrlNext());
        $this->tester->assertEquals('http://example.org?par=value', $renderer->getUrlFirst());
        $this->tester->assertEquals('http://example.org?p=90&par=value', $renderer->getUrlLast());

        $renderer = new Renderer($pager, 'http://example.org', Constants::POSITION_VARIABLE, Constants::POSITION_AS_STRING);
        $renderer->setQueryParams(['par' => 'value']);
        $this->tester->assertEquals('http://example.org/p-41?par=value', $renderer->getUrlPrev());
        $this->tester->assertEquals('http://example.org/p-60?par=value', $renderer->getUrlNext());
        $this->tester->assertEquals('http://example.org?par=value', $renderer->getUrlFirst());
        $this->tester->assertEquals('http://example.org/p-90?par=value', $renderer->getUrlLast());

        $renderer = new Renderer($pager, 'http://example.org', Constants::POSITION_VARIABLE, Constants::POSITION_AS_DIRECTORY);
        $renderer->setQueryParams(['par' => 'value']);
        $this->tester->assertEquals('http://example.org/p/41?par=value', $renderer->getUrlPrev());
        $this->tester->assertEquals('http://example.org/p/60?par=value', $renderer->getUrlNext());
        $this->tester->assertEquals('http://example.org?par=value', $renderer->getUrlFirst());
        $this->tester->assertEquals('http://example.org/p/90?par=value', $renderer->getUrlLast());

        // last page
        $pager = new Calculator(100, 91, 10);
        $renderer = new Renderer($pager, 'http://example.org');
        $this->tester->assertEquals('', $renderer->getUrlLast());

        //  position out of range
        $renderer = new Renderer($pager, 'http://example.org');
        $this->tester->assertEquals('', $renderer->getUrlLast());
    }

    public function testGetLinks(): void
    {
        $pager = new Calculator(0, 0, 10);
        $renderer = new Renderer($pager, 'http://example.org');
        $this->tester->assertEquals('', $renderer->getLinks());

        $pager = new Calculator(30, 0, 10);
        $renderer = new Renderer($pager, 'http://example.org');
        $expect = '<a href="http://example.org" class="selected" aria-current="page">1</a> <a href="http://example.org?p=10" class="" >2</a> <a href="http://example.org?p=20" class="" >3</a>';
        $this->tester->assertEquals($expect, $renderer->getLinks());

        $pager = new Calculator(30, 1, 10);
        $renderer = new Renderer($pager, 'http://example.org', Constants::POSITION_VARIABLE, Constants::POSITION_AS_STRING);
        $renderer->setQueryParams(['par' => 'value', 'par2' => 'value']);
        $expect = '<a href="http://example.org?par=value&par2=value" class="" >1</a> <a href="http://example.org/p-10?par=value&par2=value" class="" >2</a> <a href="http://example.org/p-20?par=value&par2=value" class="" >3</a>';
        $this->tester->assertEquals($expect, $renderer->getLinks());

        $pager = new Calculator(30, 1, 10);
        $renderer = new Renderer($pager, 'http://example.org', Constants::POSITION_VARIABLE, Constants::POSITION_AS_DIRECTORY);
        $renderer->setQueryParams(['par' => 'value', 'par2' => 'value']);
        $expect = '<a href="http://example.org?par=value&par2=value" class="" >1</a> <a href="http://example.org/p/10?par=value&par2=value" class="" >2</a> <a href="http://example.org/p/20?par=value&par2=value" class="" >3</a>';
        $this->tester->assertEquals($expect, $renderer->getLinks());

        $pager = new Calculator(30, 1, 10);
        $renderer = new Renderer($pager, 'http://example.org');
        $expect = '<a href="http://example.org" class="" >1</a> <a href="http://example.org?p=10" class="" >2</a> <a href="http://example.org?p=20" class="" >3</a>';
        $this->tester->assertEquals($expect, $renderer->getLinks());

        $pager = new Calculator(30, 10, 10);
        $renderer = new Renderer($pager, 'http://example.org', 'position');
        $renderer->setCssLinkSelected('customclass');
        $renderer->setLinksDelimiter(' | ');
        $expect = '<a href="http://example.org" class="" >1</a> | <a href="http://example.org?position=10" class="customclass" aria-current="page">2</a> | <a href="http://example.org?position=20" class="" >3</a>';
        $this->tester->assertEquals($expect, $renderer->getLinks());

        $pager = new Calculator(20, 10, 1);
        $renderer = new Renderer($pager, 'http://example.org');
        $expect = '<a href="http://example.org?p=9" class="" >10</a> <a href="http://example.org?p=10" class="selected" aria-current="page">11</a> <a href="http://example.org?p=11" class="selected" aria-current="page">12</a>';
        $this->tester->assertEquals($expect, $renderer->getLinks(1));
    }

    public function testGetOptions(): void
    {
        $pager = new Calculator(0, 0, 10);
        $renderer = new Renderer($pager, 'http://example.org');
        $this->tester->assertEquals('', $renderer->getOptions());

        $pager = new Calculator(30, 0, 10);
        $renderer = new Renderer($pager, 'http://example.org', 'position');
        $expect = '<option value="http://example.org" selected="selected">1</option><option value="http://example.org?position=10">2</option><option value="http://example.org?position=20">3</option>';
        $this->tester->assertEquals($expect, $renderer->getOptions());

        $pager = new Calculator(30, 21, 20);
        $renderer = new Renderer($pager, 'http://example.org');
        $expect = '<option value="http://example.org">1</option><option value="http://example.org?p=20" selected="selected">2</option>';
        $this->tester->assertEquals($expect, $renderer->getOptions());

        $pager = new Calculator(20, 10, 1);
        $renderer = new Renderer($pager, 'http://example.org');
        $expect = '<option value="http://example.org?p=9">10</option>' .
            '<option value="http://example.org?p=10" selected="selected">11</option>' .
            '<option value="http://example.org?p=11">12</option>';
        $this->tester->assertEquals($expect, $renderer->getOptions(1));

        $expect = '<option value="9">10</option>' .
            '<option value="10" selected="selected">11</option>' .
            '<option value="11">12</option>';
        $this->tester->assertEquals($expect, $renderer->getOptions(1, true));
    }
}
