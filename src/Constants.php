<?php

declare(strict_types=1);

namespace ZdenekGebauer\Pagination;

class Constants
{
    /**
     * type of pagination (?pos=10)
     */
    public const POSITION_AS_PARAMETER = 0;

    /**
     * type of pagination (/pos/10)
     */
    public const POSITION_AS_DIRECTORY = 1;

    /**
     * type of pagination (/pos-10)
     */
    public const POSITION_AS_STRING = 2;

    /**
     * name of variable holding position
     */
    public const POSITION_VARIABLE = 'p';
}
