<?php

declare(strict_types=1);

namespace ZdenekGebauer\Pagination;

class ParserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testIsPositionOutOfRange(): void
    {
        $this->tester->assertTrue(Parser::isPositionOutOfRange(92, 10, 100));
        $this->tester->assertFalse(Parser::isPositionOutOfRange(91, 10, 100));
    }

    public function testParsePosition(): void
    {
        $this->tester->assertEquals(1, Parser::parsePosition('http://example.org'));
        $this->tester->assertEquals(1, Parser::parsePosition('http://example.org/?q=11'));

        // position as parameter
        $url = 'http://example.org?p=5&amp;param=val';
        $this->tester->assertEquals(5, Parser::parsePosition($url));
        $this->tester->assertEquals(5, Parser::parsePosition($url, 9999));
        $url = 'http://example.php?position=10&param=val';
        $this->tester->assertEquals(10, Parser::parsePosition($url, Constants::POSITION_AS_PARAMETER, 'position'));

        // position as directory
        $url = 'http://example.org/p/5?param=val&amp;param2=val';
        $this->tester->assertEquals(5, Parser::parsePosition($url, Constants::POSITION_AS_DIRECTORY));
        $url = 'http://example.org/position/10?param=val';
        $this->tester->assertEquals(10, Parser::parsePosition($url, Constants::POSITION_AS_DIRECTORY, 'position'));

        // position as string
        $url = 'http://example.org/p-5?param=val&amp;param2=val';
        $this->tester->assertEquals(5, Parser::parsePosition($url, Constants::POSITION_AS_STRING));
        $url = 'http://example.org/position-10?param=val&amp;param2=val';
        $this->tester->assertEquals(10, Parser::parsePosition($url, Constants::POSITION_AS_STRING, 'position'));
    }
}
