<?php

declare(strict_types=1);

namespace ZdenekGebauer\Pagination;

class Calculator
{

    /**
     * @var int
     */
    protected $totalRows;

    /**
     * @var int
     */
    protected $currentPosition;

    /**
     * @var int
     */
    protected $rowsPage;

    /**
     * array with positions for each page
     * @var array<int, int>
     */
    protected $pages = [];

    /**
     * @var int
     */
    protected $currentPage;

    /**
     * @var int
     */
    protected $currentPageNo = 0;

    /**
     * @var int
     */
    protected $prevPosition = 0;

    /**
     * @var int
     */
    protected $nextPosition = 0;

    /**
     * end of current page
     * @var int
     */
    protected $endPosition = 0;

    /**
     * start position of last page
     * @var int
     */
    protected $lastPosition = 0;

    /**
     * @param int $totalRows total number of records
     * @param int $currentPos current position in list
     * @param int $rowsPage records per page [1-1000]
     */
    public function __construct(int $totalRows, int $currentPos = 0, int $rowsPage = 10)
    {
        $this->totalRows = max([0, $totalRows]);
        $this->currentPosition = max([0, $currentPos]);
        $this->rowsPage = min([1000, max([0, $rowsPage])]);
        $this->recalculate();
    }

    private function recalculate(): void
    {
        $this->pages = [];
        $pageCounter = 0;
        for ($counter = 0; $counter < $this->totalRows; $counter += $this->rowsPage) {
            $this->pages[] = $counter;
            if ($counter <= $this->currentPosition) {
                $this->currentPage = $counter;
                $this->currentPageNo = $pageCounter;
            }
            $pageCounter++;
        }
        $this->lastPosition = (empty($this->pages) ? 0 : (int)end($this->pages));
        $this->endPosition = min([$this->currentPosition + $this->rowsPage, $this->totalRows]);

        $this->prevPosition = max([0, $this->currentPosition - $this->rowsPage + 1]);

        $this->nextPosition = $this->currentPosition + $this->rowsPage;
        $this->nextPosition = ($this->nextPosition > $this->lastPosition) ? 0 : $this->nextPosition;

        if ($this->currentPosition >= ($this->lastPosition - $this->rowsPage) || empty($this->pages)) {
            $this->lastPosition = 0;
        }
    }

    /**
     * returns current position in list
     * @return int
     */
    public function getCurrentPosition(): int
    {
        return $this->currentPosition;
    }

    /**
     * @return int
     */
    public function getCurrentPageNo(): int
    {
        return $this->currentPageNo;
    }

    /**
     * returns array of positions for pages
     *
     * @return array<int, int>
     */
    public function getPages(): array
    {
        if (!isset($this->pages[0])) {
            $this->recalculate();
        }
        return $this->pages;
    }

    /**
     * returns array with positions of current page (first, last record)
     * @return array<int, int>
     */
    public function getRecords(): array
    {
        $begin = $this->currentPage;
        $end = $begin + $this->rowsPage - 1;
        $end = (($end > $this->totalRows) ? $this->totalRows : $end);
        return [$begin, $end];
    }

    /**
     * returns end position of current page
     *
     * @return int
     */
    public function getEndPosition(): int
    {
        return $this->endPosition;
    }

    /**
     * Returns position of previous page. Returns 0 if curent page is first page
     *
     * @return int
     */
    public function getPrevPosition(): int
    {
        return $this->prevPosition;
    }

    /**
     * Returns position of next page. Returns 0 if curent page is last page
     *
     * @return int
     */
    public function getNextPosition(): int
    {
        return $this->nextPosition;
    }

    /**
     * Returns position of next page. Returns 0 if curent page is last page
     *
     * @return int
     */
    public function getLastPosition(): int
    {
        return $this->lastPosition;
    }
}
